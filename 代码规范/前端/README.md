# 前端开发规范

目前前端项目具有一些公共注解方法：

**注:`autoLoading`与`processError`设置一个就行，在使用了`autoLoading`时就无需使用`processError`**

- `autoLoading(key)` 在组件内调用使用该注解的方式时，将会自动设置传递给该方法的`key`的值，正在执行时为`true`执行完成更改为`false`，一般用于数据请求时，设置loading状态；在调用使用设置的方法时，如果被调用的方法有错误，将会自动把错误信息显示出来
- `elConfirm(title, message, options)` 确认框注解
- `processError` 错误处理注解

全局mixin里面的一些方法

- `showDialogByRefName(refName, showMethodName, ...args)` 通过ref名称和显示弹窗的方法来显示某个弹窗

## 项目结构

前端项目部分目录结构
```
|- /api                     API文件
|- /components              可复用的基础组件
|- /components-business     可复用的带有业务逻辑的组件 (如果一个组件设计到业务, 并且会被多个其它页面所使用, 放这里)
|- /config                  第三方包及一些配置
|- /decorator               全局的注解
|- /directives              自定义指令
|- /filter                  自定义过滤器 ({{ revision | setRevision }})
|- /mixins                  全局mixins
|- /route                   页面路由
|- /store                   Vuex文件
|- /views                   具体的页面
```

**页面的文件结构组织方式**

```
views/kanban/                                   页面名称
|- components/                                  当前页面的公共组件
|- components/DialogIssueDetail.vue             任务详情
|- pages/                                       当前页面下的子页面
|- pages/backlog/
|- pages/backlog/components/ 
|- pages/backlog/index.vue                      backlog子页面
|- index.vue                                    kanban的入口页面
```


## 代码规范

在遵循已经设置的Eslint的基本规则的同时，还需要注意以下规范：

- [必须] 使用第三方组件或者自定义组件时一律使用`kebab-case`
- [必须] 保持`template`模板简单，单个vue文件内容不能过大，能够拆分成组件的拆分成组件
- [必须] 在模板不能进行数据处理，保持模板简单整洁，数据处理一律放在JS里面
- [必须] vue文件的生命周期方法顺序`[name,components,props,data,computed,created,mounted,activited,update,beforeRouteUpdate,metods,watch]`
- [必须] 普通表格页面使用`pageMixin`，弹窗使用`dialogMixin`，后续会开发公共的弹窗组件，以及公共的页面组件
- [必须] 所有API对象以`API`开头，后面以业务名称结束，全部采用大写下划线方式；每个API方法里面的参数需要使用jsDoc来标注参数的名称，类型，以及描述
- [必须] 标签名自闭合：如果一个组件或者元素内，没有其他子元素，使用自闭合 <my-components/>
- [必须] components 里面的组件统一使用单词大写开头，并且组件名应该以高级别的 (通常是一般化描述的) 单词开头，以描述性的修饰词结尾

    ```
    components/
    |- SearchButtonClear.vue
    |- SearchButtonRun.vue
    |- SearchInputQuery.vue
    |- SearchInputExcludeGlob.vue
    |- SettingsCheckboxTerms.vue
    |- SettingsCheckboxLaunchOnStartup.vue
    ```
- [推荐] 优先使用`Vuex`尽量不要使用`this.$root`