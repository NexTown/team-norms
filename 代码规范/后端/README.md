# Java编码规范

## 编码规范

- 使用linux换行符。
- 缩进（包含空行）和上一行保持一致。
- 类声明后与下面的变量或方法之间需要空一行。
- 捕获的异常名称命名为`ex`；捕获异常且不做任何事情，异常名称命名为`ignored`。
- 需要注释解释的代码尽量提成小方法，用方法名称解释。
- `equals`和`==`条件表达式中，常量在左，变量在右；大于小于等条件表达式中，变量在左，常量在右。
- 除了构造器入参与全局变量名称相同的赋值语句外，避免使用`this`修饰符。
- 嵌套循环尽量提成方法。
- 优先使用卫语句。
- 类和方法的访问权限控制为最小。
- 方法入参和返回值不允许为`null`。
- 优先使用三目运算符代替if else的返回和赋值语句。
- 优先考虑使用`LinkedList`，只有在需要通过下标获取集合中元素值时再使用`ArrayList`。
- `ArrayList`，`HashMap`等可能产生扩容的集合类型必须指定集合初始大小，避免扩容。
- 公开的类和方法必须有javadoc，其他类和方法以及覆盖自父类的方法无需javadoc。javadoc一定要对方法参数、返回值、异常抛出等进行描述。但如果是重写父类方法或实现接口方法可以忽略javadoc。
- 方法参数控制在5个以下，超过5个就使用对象代替。
- 不能出现魔法数字。
- 非特殊情况不要使用包装类型，而是使用基本类型。


## 其它说明
### 卫语句
卫语句就是把复杂的条件表达式拆分成多个条件表达式，比如一个很复杂的表达式，嵌套了好几层的if - then-else语句，转换为多个if语句，实现它的逻辑，这多条的if语句就是卫语句。例如下面的例子：
```
if（obj != null）{
  doSomething();
}
// 转换成卫语句以后的代码如下：
if(obj == null){
   return;
}
doSomething();
```

### QueryDSL更新数据
更新字段不能查询出实体后对其中的属性重新赋值后再save。可以直接使用`JPAQueryFactory`的`update`方法更新。
```
getQueryFactory().update(qStudent)
    .set(qStudent.name, "Jack")
    .where(qStudent.id.eq(1))
    .execute();

// 如果删除数据不是根据主键删除, 可以通过下面的写法快速完成
getQueryFactory().delete(qStudent).where(qStudent.name.eq("Jack")).execute();
```

## Idea 配置
### tab键设置为4个空格
**File->Setting(Project Settings)->Editor>Code Style->Java**

`Use tab character`不要选，然后`indent`设置为4，代表按一个tab为4个空格

### 每行缩进显示小点表示
**File>Setting>Editor>General>Appearance**

勾选 `Show whitespaces`

### 禁用 import *
**File>Setting>Editor>Code Style>Java**

接着选择 Imports 标签卡，将 `Class count to use import with '*'` 和其下面一项 的值设置为 999或更大。

### CheckStyle 配置
参照项目 checkstyle 目录中 `README.md` 文件进行配置。

