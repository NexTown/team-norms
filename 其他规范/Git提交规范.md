# Git提交规范

规范commit message的好处：

- 方便快速浏览查找。回溯之前的工作内容
- 可以直接从commit生产change log

commit格式：

要保证每次commit是一次完整的提交，具有系统任务的提交需要带上任务编号

```
<type>([#issueID],[scope]): <subject> 
```
```
 ⭐`:star:`                       新功能、新模块
 🐛`:bug:`                        本地/开发修补bug
 🚑`:ambulance:`                  线上分支bug修复(prod)
 📝`:memo:`                       添加/修改文档
 🎨`:art:`                        UI样式相关
 ♻`:recycle:`                     重构代码, 代码优化相关 (包括代码格式/性能上面的优化, 不影响现有功能的情况下的优化)
 ✅`:white_check_mark:`           增加测试
 🔧`:wrench:`                     构建过程或辅助工具的变动, 配置/部署相关 文件新增或修改
 🍭`:lollipop:`                   模块下功能的补齐和完善
```
如果是处理任务系统中的任务, 需要跟上任务编号, 格式为 `:bug: #任务编号  其它描述`
